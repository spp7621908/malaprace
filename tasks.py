# --8<-- [start:imports]
"""
Imports
"""
import dataclasses
from typing import Callable, Generic, List, Optional, TypeVar
# --8<-- [end:imports]


# --8<-- [start:cached]
def cached(f):
    """
    Create a decorator that caches up to 3 function results, based on the same parameter values.

    When `f` is called with the same parameter values that are already in the cache, return the
    stored result associated with these parameter values. You can assume that `f` receives only
    positional arguments (you can ignore keyword arguments).

    When `f` is called with new parameter values, forget the oldest inserted result in the cache
    if the cache is already full.

    Example:
        @cached
        def fn(a, b):
            return a + b # imagine an expensive computation

        fn(1, 2) == 3 # computed
        fn(1, 2) == 3 # returned from cache, `a + b` is not executed
        fn(3, 4) == 7 # computed
        fn(3, 5) == 8 # computed
        fn(3, 6) == 9 # computed, (1, 2) was now forgotten
        fn(1, 2) == 3 # computed again, (3, 4) was now forgotten
    """
    cache = {}
    def inner(*args):
        if args in cache:
            return cache[args]
        if len(cache) == 3:
            cache.pop(list(cache.keys())[0])
        cache[args] = f(*args)
        return cache[args]
    return inner
# --8<-- [end:cached]


# --8<-- [start:TypeVar]
T = TypeVar("T")
# --8<-- [end:TypeVar]

# --8<-- [start:ParseResult]
@dataclasses.dataclass
class ParseResult(Generic[T]):
    """
    Represents result of a parser invocation.
    If `value` is `None`, then the parsing was not successful.
    `rest` contains the rest of the input string if parsing was succesful.
    """
    value: Optional[T]
    rest: str

    @staticmethod
    def invalid(rest: str) -> "ParseResult":
        """
        Check validity
        """
        return ParseResult(value=None, rest=rest)

    def is_valid(self) -> bool:
        """
        Check validity
        """
        return self.value is not None
# --8<-- [end:ParseResult]

"""
Represents a parser: a function that takes a string as an input and returns a `ParseResult`.
"""
# --8<-- [start:Parser]
Parser = Callable[[str], ParseResult[T]]
# --8<-- [end:Parser]

"""
Below are functions that create new parsers.
They should serve as LEGO blocks that can be combined together to build more complicated parsers.
See tests for examples of usage.

Note that parsers are always applied to the beginning of the string:
```python
parser = parser_char("a")
parser("a")  # ParseResult(value="a", rest="")
parser("xa") # ParseResult(value=None, rest="xa")
```
"""

# --8<-- [start:parser_char]
def parser_char(char: str) -> Parser[str]:
    """
    Return a parser that will parse a single character, `char`, from the beginning of the input
    string.

    Example:
        ```python
        parser_char("x")("x") => ParseResult(value="x", rest="")
        parser_char("x")("xa") => ParseResult(value="x", rest="a")
        parser_char("y")("xa") => ParseResult(value=None, rest="xa")
        ```
    """
    if len(char) > 1 or not char:
        raise ValueError()

    def parse(input: str):

        if input and input[0] == char:
            return ParseResult(value=char, rest=input[len(char):])

        return ParseResult.invalid(input)

    return parse
# --8<-- [end:parser_char]


# --8<-- [start:parser_repeat]
def parser_repeat(parser: Parser[T]) -> Parser[List[T]]:
    """
    Return a parser that will invoke `parser` repeatedly, while it still matches something in the
    input.

    Example:
        ```python
        parser_a = parser_char("a")
        parser = parser_repeat(parser_a)
        parser("aaax") => ParseResult(value=["a", "a", "a"], rest="x")
        parser("xa") => ParseResult(value=[], rest="xa")
        ```
    """
    def inner(s):
        result = []
        while True:
            r = parser(s)
            if r.is_valid():
                result.append(r.value)
                s = r.rest
            else:
                return ParseResult(value=result, rest=s)
    return inner
# --8<-- [end:parser_repeat]


# --8<-- [start:parser_seq]
def parser_seq(parsers: List[Parser]) -> Parser:
    """
    Create a parser that will apply the given `parsers` successively, one after the other.
    The result will be successful only if all parsers succeed.

    Example:
        ```python
        parser_a = parser_char("a")
        parser_b = parser_char("b")
        parser = parser_seq([parser_a, parser_b, parser_a])
        parser("abax") => ParseResult(value=["a", "b", "a"], rest="x")
        parser("ab") => ParseResult(value=None, rest="ab")
        ```
    """
    def seq(input: str):
        values = []
        rest = input

        for parser in parsers:
            result = parser(rest)
            values.append(result.value)
            rest = result.rest

            if not result.is_valid():
                return ParseResult.invalid(input)

        return ParseResult(values, rest)

    return seq
# --8<-- [end:parser_seq]


# --8<-- [start:parser_choice]
def parser_choice(parsers: List[Parser]) -> Parser:
    """
    Return a parser that will return the result of the
    first parser in `parsers` that matches something
    in the input.

    Example:
        ```python
        parser_a = parser_char("a")
        parser_b = parser_char("b")
        parser = parser_choice([parser_a, parser_b])
        parser("ax") => ParseResult(value="a", rest="x")
        parser("bx") => ParseResult(value="b", rest="x")
        parser("cx") => ParseResult(value=None, rest="cx")
        ```
    """
    def anyof(input: str):
        for parser in parsers:
            result = parser(input)

            if result.is_valid():
                return result

        return ParseResult.invalid(input)

    return anyof
# --8<-- [end:parser_choice]




# --8<-- [start:R]
R = TypeVar("R")
# --8<-- [end:R]


# --8<-- [start:parser_map]
def parser_map(parser: Parser[R], map_fn: Callable[[R], Optional[T]]) -> Parser[T]:
    """
    Return a parser that will use `parser` to parse the input data, and if it is successful, it will
    apply `map_fn` to the parsed value.
    If `map_fn` returns `None`, then the parsing result will be invalid.

    Example:
        ```python
        parser_a = parser_char("a")
        parser = parser_map(parser_a, lambda x: x.upper())
        parser("ax") => ParseResult(value="A", rest="x")
        parser("bx") => ParseResult(value=None, rest="bx")

        parser = parser_map(parser_a, lambda x: None)
        parser("ax") => ParseResult(value=None, rest="ax")
        ```
    """
    def ma(input: str):
        result = parser(input)

        if result.is_valid():
            map_result = map_fn(result.value)

            if map_result is None:
                return ParseResult.invalid(input)

            return ParseResult(map_result, result.rest)

        return ParseResult.invalid(input)

    return ma
# --8<-- [end:parser_map]


# --8<-- [start:parser_matches]
def parser_matches(filter_fn: Callable[[str], bool]) -> Parser[str]:
    """
    Create a parser that will parse the first character from the input, if it is accepted by the
    given `filter_fn`.

    Example:
        ```python
        parser = parser_matches(lambda x: x in ("ab"))
        parser("ax") => ParseResult(value="a", rest="x")
        parser("bx") => ParseResult(value="b", rest="x")
        parser("cx") => ParseResult(value=None, rest="cx")
        parser("") => ParseResult(value=None, rest="")
        ```
    """
    def match(input: str):
        if not input:
            return ParseResult.invalid(input)

        if filter_fn(input[0]):
            return ParseResult(input[0], input[1:])

        return ParseResult.invalid(input)

    return match
# --8<-- [end:parser_matches]


# --8<-- [start:parser_string]
def parser_string(string: str) -> Parser[str]:
    """
    Create a parser that will parse the given `string`.

    Example:
        ```python
        parser = parser_string("foo")
        parser("foox") => ParseResult(value="foo", rest="x")
        parser("fo") => ParseResult(value=None, rest="fo")
        ```
    """
    def parse(input: str):
        letters = list(string)
        parsers = []

        if not string:
            return ParseResult("", input)

        for c in letters:
            parsers.append(parser_char(c))

        result = parser_seq(parsers)
        result = result(input)

        if result.value:
            result.value = ''.join(result.value)

        return ParseResult(result.value, result.rest)

    return parse
# --8<-- [end:parser_string]


# --8<-- [start:parser_int]
def parser_int() -> Parser[int]:
    """
    Create a parser that will parse a non-negative integer (you don't have to deal with
    `-` at the beginning).

    Example:
        ```python
        parser = parser_int()
        parser("123x") => ParseResult(value=123, rest="x")
        parser("foo") => ParseResult(value=None, rest="foo")
        ```
    """
    def parse(input: str):

        parser = parser_matches(lambda x: x in ("0123456789"))
        rest = input
        number = ''

        for _ in input:
            result = parser(rest)

            if not result.is_valid():
                break

            rest = result.rest
            number += result.value

        try:
            result.value = int(number)
            return result
        except:
            return result

    return parse
# --8<-- [end:parser_int]


print('Tasks.py has started')
parser_string("xxab")
parser_string("xxab")
parser_string("xxab")
parser_string("xxab")
print('Tasks.py has ended')
