# Funkcionální programování v Pythonu

Ukážeme si, jak pracovat s funkcemi, iterátory, generátory a dekorátory.

Odpovídající materiály naleznete níže.  
[PyLadies Course](https://naucse.python.cz/course/pyladies/)  
[Nauč se Python](https://naucse.python.cz/course/mi-pyt/advanced/generators/)  

``` py title="Imports" linenums="1"
--8<-- "./tasks.py:imports"
```

``` py title="Dekorátor chached" linenums="1"
--8<-- "./tasks.py:cached"
```

``` py title="TypeVar" linenums="1"
--8<-- "./tasks.py:TypeVar"
```

``` py title="ParseResult" linenums="1"
--8<-- "./tasks.py:ParseResult"
```

``` py title="Parser" linenums="1"
--8<-- "./tasks.py:Parser"
```

``` py title="parser_char" linenums="1"
--8<-- "./tasks.py:parser_char"
```

``` py title="parser_repeat" linenums="1"
--8<-- "./tasks.py:parser_repeat"
```

``` py title="parser_seq" linenums="1"
--8<-- "./tasks.py:parser_seq"
```

``` py title="parser_choice" linenums="1"
--8<-- "./tasks.py:parser_choice"
```

``` py title="R" linenums="1"
--8<-- "./tasks.py:R"
```

``` py title="parser_map" linenums="1"
--8<-- "./tasks.py:parser_map"
```

``` py title="parser_matches" linenums="1"
--8<-- "./tasks.py:parser_matches"
```

``` py title="parser_string" linenums="1"
--8<-- "./tasks.py:parser_string"
```

``` py title="parser_int" linenums="1"
--8<-- "./tasks.py:parser_int"
```

